from setuptools import setup
import os

setup(name='my_package',
      version=os.environ['CI_COMMIT_TAG'],
      description='Some Python package',
      author='Me',
      author_email='me@example.com',
      license='MIT',
      packages=['my_package'],
      zip_safe=False)